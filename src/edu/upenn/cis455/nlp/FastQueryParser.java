/**
 * 
 */
package edu.upenn.cis455.nlp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

import opennlp.tools.stemmer.PorterStemmer;

/**
 * @author Jatin Sharma
 */
public class FastQueryParser extends QueryParser{
	
	private PorterStemmer stemmer;
	private TreeSet<String> stopwords;
	private final String englishStopWords[] = {
			 "a", "an", "and", "are", "as", "at", "be", "but", "by",
			 "for", "if", "in", "into", "is", "it",
			 "no", "not", "of", "on", "or", "such",
			 "that", "the", "their", "then", "there", "these",
			 "they", "this", "to", "was", "will", "with"
			 };

	public FastQueryParser(){
		
		// load stemmer
		this.stemmer = new PorterStemmer();
		
		// load stopword cleaner
		this.stopwords = new TreeSet<String>();
		for(int i=0; i< englishStopWords.length;i++)
			this.stopwords.add(englishStopWords[i]);
	}
	
    //This method defines a generic structure for parsing the query
    public String[] getKeywords(String searchQuery){

    	String[] searchKeywords = getTokens(searchQuery);
    	searchKeywords = removeStopWords(searchKeywords);
    	searchKeywords = spellCorrect(searchKeywords);
    	searchKeywords = getStems(searchKeywords);
    	
    	return searchKeywords;
    }
    
	String[] getTokens(String searchString) {
		return searchString.replaceAll("[^a-zA-Z ]", "").toLowerCase().split("\\s+");
	}

	String[] removeStopWords(String[] searchKeywords) {
		ArrayList<String> newTokens = new ArrayList<String>(Arrays.asList(searchKeywords));
		for (int i=0;i<newTokens.size();i++){
			if (this.stopwords.contains(newTokens.get(i)))
				newTokens.remove(i);
		}
		return (String[])newTokens.toArray(new String[newTokens.size()]);
	}
	
	public String[] getStems(String[] searchKeywords){
		String[] searchStems = new String[searchKeywords.length];
		for(int i=0; i<searchKeywords.length; i++){
			searchStems[i] = this.stemmer.stem(searchKeywords[i]); 
		}
		return searchStems;
	}

	String[] spellCorrect(String[] searchKeywords) {
		// TODO Auto-generated method stub
		return searchKeywords;
	}

	List<String> getSuggestions(String searchQuery) {
		
		// Declare the list 
		List<String> suggestedQueryList = new ArrayList<String>();
		
		// TODO prepare some suggestions and add to the list
		// suggestedQueryList.add(suggestion);
		
		return suggestedQueryList;
	}
    
	public static void main(String[] args){
		
		FastQueryParser parser = new FastQueryParser();
		String searchString = "how to do your first operation by John McCaley + Google Inc, San Jose?";
		
		String[] searchTokens = parser.getTokens(searchString);
		String[] searchTagsNoStopWords = parser.removeStopWords(searchTokens);
		

		System.out.println("searchString \t: "+searchString);
		System.out.println("searchTokens \t: "+Arrays.toString(searchTokens));
		System.out.println("searchTagsNoStopWords \t: "+Arrays.toString(searchTagsNoStopWords));
		
//		Object lock = new Object();
//		String[] searchKeywords = null;
//		long start = System.nanoTime();
//		for(int i=0; i<1000000; i++){
//			synchronized(lock){
//				searchKeywords = parser.getKeywords(searchString);
//			}
//		}
//		System.out.println("searchKeywords \t: "+Arrays.toString(searchKeywords));
//		System.out.println((System.nanoTime()-start)/(1000000*1000) + " us");

	}
}

