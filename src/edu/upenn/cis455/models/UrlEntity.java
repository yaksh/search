package edu.upenn.cis455.models;

/**
 * @author cis455
 *
 */
public class UrlEntity implements Comparable<UrlEntity>{
	
	private String url;
	private double score;
	
	public UrlEntity() {
		// TODO Auto-generated constructor stub
	}
	
	public UrlEntity(String url, double score) {
		this.url = url;
		this.score = score;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	
	@Override
	public int compareTo(UrlEntity o) {
		if(this.score < o.score)
			return 1;
		else if(this.score > o.score)
			return -1;
		else
			return 0;
	}
}
