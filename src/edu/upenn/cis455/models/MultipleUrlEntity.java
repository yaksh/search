package edu.upenn.cis455.models;

public class MultipleUrlEntity implements Comparable<MultipleUrlEntity>{
	
	private double score;
	private long count;
	private String url;
	
	/**
	 * @param score
	 * @param count
	 */
	public MultipleUrlEntity(String url, double score, long count) {
		this.url = url;
		this.score = score;
		this.count = count;
	}

	@Override
	public int compareTo(MultipleUrlEntity o) {
		int firstComp = 0;
		if(this.count < o.count)
			firstComp = 1;
		else if(this.count > o.count)
			firstComp = -1;
		else
			firstComp = 0;
		
		if(firstComp != 0)
			return firstComp;
		else{
			if(this.score < o.score)
				return 1;
			else if(this.score > o.score)
				return -1;
			else
				return 0;
		}
	}
	
	public void increment(){
		this.count++;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
