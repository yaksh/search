package edu.upenn.cis455.search;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JSServlet extends HttpServlet{
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		BufferedReader reader = null;
		try{
			resp.setContentType("text/javascript");
			String fileRequested = req.getServletPath();
			PrintWriter writer = resp.getWriter();
//			ServletContext context = getServletContext();
//			URL filePath = context.getResource("" + fileRequested);
			String filePath = "/opt/jetty/webapps/ROOT" + fileRequested;
			String line = "";
			String content = "";
			reader = new BufferedReader(new FileReader(new File(filePath)));
			while((line = reader.readLine()) != null){
				content += line;
			}
			writer.println(content);
		}
		catch(Exception e){
			System.err.println("Couldn't find the resource");
		}
		finally {
			if(reader != null){
				reader.close();
			}
		}
	}
}
