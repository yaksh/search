package edu.upenn.cis455.search;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.upenn.cis455.models.MultipleUrlEntity;
import edu.upenn.cis455.models.UrlEntity;
import edu.upenn.cis455.nlp.FastQueryParser;

/**
 * @author cis455
 *
 */
public class SearchResultsServlet extends HttpServlet{
	
	//3571 Urls for now
	private HashMap<String, List<UrlEntity>> cachedWords = new HashMap<>();
	public static HashMap<String, MultipleUrlEntity> unionUrls = new HashMap<>();
	public static String awsAccessKey;
	public static String awsSecretKey;
	public static final long totalUrls = 100700;
	public static final long totalCachedUrls = 3571 * 1;
	public static final int totalUrlsFetch = 1000;
	
	public void init(ServletConfig servletConfig) throws ServletException{
		super.init(servletConfig);
		setup();
	    SearchResultsServlet.awsAccessKey = servletConfig.getInitParameter("awsAccessKey");
	    SearchResultsServlet.awsSecretKey = servletConfig.getInitParameter("awsSecretKey");
	  }
	
	/*
	 * Serves request
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		try{
			
			long startTime = System.currentTimeMillis();
			ArrayList<MultipleUrlEntity> finalResults = new ArrayList<>();
			unionUrls.clear();
			String words = req.getParameter("keywords");
			FastQueryParser parser = new FastQueryParser();
			String[] keyWords = parser.getKeywords(words);
			String links = "";
			resp.setContentType("text/html");
			
			FetchResults results = new FetchResults(keyWords);
			
			for(String key : keyWords){
				if(cachedWords.containsKey(key)){
					for(UrlEntity url : cachedWords.get(key)){
						if(unionUrls.containsKey(url.getUrl())){
							MultipleUrlEntity m = unionUrls.get(url.getUrl());
							m.increment();
							unionUrls.put(url.getUrl(), m);
						}
						else{
							unionUrls.put(url.getUrl(), new MultipleUrlEntity(url.getUrl(), url.getScore(), 1));
						}
					}
				}
			}
			
			for(String u : unionUrls.keySet()){
				finalResults.add(unionUrls.get(u));
			}
			Collections.sort(finalResults);

			String toDisplay = "";
			int fetchedurl = 0;
			for(MultipleUrlEntity multipleEntry : finalResults){
				if(fetchedurl >= totalUrlsFetch)
					break;
//				links += "<div class='results well'><strong>Link</strong><p><a href='" + multipleEntry.getUrl() + "'>"+ multipleEntry.getUrl() + "Score: " + multipleEntry.getScore() + "Count: " + multipleEntry.getCount() + "</a></div>";
				if(multipleEntry.getUrl().length() > 50){
					toDisplay = multipleEntry.getUrl().substring(0, 50);
				}
				else {
					toDisplay = multipleEntry.getUrl();
				}
				
				String domain = "";
				try{
					URI uri = new URI(multipleEntry.getUrl());
				    domain = uri.getHost();
				    domain = domain.startsWith("www.") ? domain.substring(4) : domain;
				}
				catch(Exception e){
					domain = "Link";
				}
				//<div class='box'><iframe align='right' id='frame' src='" + multipleEntry.getUrl() + "'></iframe></div>
				links += "<tr><td><div class='results well'><strong>" + domain + "</strong><p><a href='" + multipleEntry.getUrl() + "'>"+ toDisplay + "</a></p></div></td></tr>";
				fetchedurl++;
			}
			long stopTime = System.currentTimeMillis();
			PrintWriter writer = resp.getWriter();
			float time = (stopTime - startTime) / 1000.0000f;
			String timeStr = new DecimalFormat("#.#####").format(time);
//			writer.println("<html><head><title>Yaksh</title></head><body><h2 align='center'>Yaksh</h2><h3 align='center'>"+ finalResults.size() +" results in "+ ((stopTime - startTime)/1000) +" sec</h3><div style='width:100%;'><div style='float:left; width:300px; margin-left:300px;'><table border=1 align='center'>"+ links +"</table></div></div></body></html>");
			//<script type='text/javascript' src='res/styles/paging.js'>
			
			writer.println("<!DOCTYPE html><html><head><link rel='stylesheet' href='res/styles/bootstrap.css' type='text/css'>"
					+ "<link rel='stylesheet' href='res/styles/results.css' type='text/css'>"
					+ "<link rel='stylesheet' href='res/styles/bootstrap-responsive.css' type='text/css'>"
					+ "<title>Yaksh</title>"
					+ "</script></head><body><div class='navbar navbar-fixed-top'>"
					+ "<div class='navbar-inner'><div class='container'><span class='brand'>"
					+ "<h2><a href='/search/'><strong class='orange'>Yaksh</strong></a></h2></span>"
					+ "<form class='navbar-form form-inline' action='searchResults' method='GET' id='searchbox'>"
					+ "<input type='text' name='keywords' class='input-xlarge' id='searchinput'><button type='submit' class='btn btn-warning'>"
					+ "<i class='icon-search icon-white'></i>&#128269;</button></form></div></div></div><div class='container'><div class='row'>"
					+ "<div class='span6'><h4>"+ finalResults.size() + " results in " + timeStr + " seconds" +"<strong class='orange'></strong>"
					+ "</h4></div></div><div class='row'><div class='span5' id='fixed'><div class='span7'><div><h4>Results for "+ words + ":</h4>"
					+ "<table id='results' class='span6'>" + links + "</table>"
					+ "<div class='orange' id='pageNavPosition'></div><br></div></div></div></div></div>"
					+ "<script type='text/javascript'>function Pager(tableName1, itemsPerPage1) {var tableName = tableName1; var itemsPerPage = itemsPerPage1; var currentPage = 1; var pages = 0; var inited = false; "
					+ "this.showRecords = function(from, to) {var rows = document.getElementById(tableName).rows; for (var i = 1; i < rows.length; i++) {if (i < from || i > to) rows[i].style.display = 'none'; else rows[i].style.display = ''; } } "
					+ "this.showPage = function(pageNumber) {if (! inited) {return; } var oldPageAnchor = document.getElementById('pg'+currentPage); oldPageAnchor.className = 'pg-normal'; currentPage = pageNumber; var newPageAnchor = document.getElementById('pg'+currentPage); newPageAnchor.className = 'pg-selected'; var from = (pageNumber - 1) * itemsPerPage + 1; var to = from + itemsPerPage - 1; this.showRecords(from, to); } "
					+ "this.prev = function() {if (currentPage > 1) showPage(currentPage - 1); } "
					+ "this.next = function() {if (currentPage < pages) {showPage(currentPage + 1); } } "
					+ "this.init = function() {var rows = document.getElementById(tableName).rows; var records = (rows.length - 1); pages = Math.ceil(records / itemsPerPage); inited = true; } "
					+ "this.showPageNav = function(pagerName, positionId) {if (! inited) { return; } "
					+ "var element = document.getElementById(positionId); var pagerHtml = '<span onclick='' + pagerName + '.prev();' class='pg-normal'> &#171 Prev </span> | '; "
					+ "for (var page = 1; page <= pages; page++) pagerHtml += '<span id='pg' + page + '' class='pg-normal' onclick='' + pagerName + '.showPage(' + page + ');'>' + page + '</span> | '; pagerHtml += '<span onclick=''+pagerName+'.next();' class='pg-normal'> Next &#187;</span>'; "
					+ "element.innerHTML = pagerHtml; } }  var pager = new Pager('results', 15); pager.init();pager.showPageNav('pager', 'pageNavPosition');pager.showPage(1);</script></body></html>");
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Initial setup
	 */
	private void setup(){
		BufferedReader reader = null;
		try{
//			ServletContext context = getServletContext();
//			System.out.println(context.getResourcePaths("/test-resources/"));
//			InputStream in = context.getResourceAsStream("/test-resources/");
//			if(in != null)
//				System.out.println("skj");
//			URL filePath = context.getResource("res/Indexer.csv");
//			String filePath = context.getRealPath("/res/Indexer.csv");
//			System.out.println(filePath.toURI().toString());
//			System.out.println(context.getRealPath("/res/Indexer.csv"));
			cachedWords = new HashMap<>();
			unionUrls = new HashMap<>();
			File cacheFile = new File("/opt/jetty/webapps/ROOT/res/Indexer.csv");
//			File cacheFile = new File("res/Indexer.csv");
			reader = new BufferedReader(new FileReader(cacheFile));
			String line = "";
			int idf = 0;
			int tf = 0;
			double score = 0.0;
			while((line = reader.readLine()) != null){
				if(line.contains("\t")){
					String[] keyValue = line.split("\t");
					if(keyValue.length == 2){
						if(keyValue[1].contains("||")){
							String[] tokens = keyValue[1].split("\\|\\|");
							if(tokens.length > 0){
								idf = tokens.length;
								ArrayList<UrlEntity> urlList = new ArrayList<>();
								for(String token : tokens){
									String[] url = token.split("\\{\\{");
									String[] positions = url[1].split(",");
									tf = positions.length;
									score = tf * (Math.log(totalCachedUrls/idf) / Math.log(10));
									UrlEntity e = new UrlEntity(url[0], score);
									urlList.add(e);
								}
								Collections.sort(urlList);
								cachedWords.put(keyValue[0], urlList);
							}
						}
					}
				}
			}
		}
		catch(Exception e){
			System.err.println("Error in loading the prefetched words");
			e.printStackTrace();
		}
		finally{
			if(reader != null){
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
