package edu.upenn.cis455.search;

import java.util.ArrayList;
import java.util.List;

import edu.upenn.cis455.models.CacheUrl;
import edu.upenn.cis455.models.LeafEntity;
import edu.upenn.cis455.models.MultipleUrlEntity;
import edu.upenn.cis455.storage.DynamoStorage;

public class FetchResults {
	
	private String[] keyWords;
	private DynamoStorage storage;
	
	public FetchResults(String[] keyWords) {
		this.keyWords = keyWords;
		storage = new DynamoStorage(SearchResultsServlet.awsAccessKey, SearchResultsServlet.awsSecretKey);
		startProccessing();
	}
	
	private void startProccessing(){
		List<Object> words = new ArrayList<>();
		for(String word: keyWords){
			LeafEntity leaf = new LeafEntity();
			leaf.setWord(word);
			words.add(leaf);
		}
		
		ArrayList<LeafEntity> results = storage.getWords(words);
		for(LeafEntity result : results){
//			System.out.println(result.getWord());
			String prevWord = result.getWord();
			for(CacheUrl url : result.getCacheUrlList()){
				if(SearchResultsServlet.unionUrls.containsKey(url.getUrl()) && !result.getWord().equals(prevWord)){
					System.out.println("repeated");
					MultipleUrlEntity m = SearchResultsServlet.unionUrls.get(url.getUrl());
					m.increment();
					SearchResultsServlet.unionUrls.put(url.getUrl(), m);
				}
				else{
					double score = url.getTF() * (Math.log(SearchResultsServlet.totalUrls/result.getIDF()) / Math.log(10));
					SearchResultsServlet.unionUrls.put(url.getUrl(), new MultipleUrlEntity(url.getUrl(), score, 1));
				}
			}
		}
	}
}
