package edu.upenn.cis455.search;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * @author cis455
 *
 */
public class SearchServlet extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		BufferedReader reader = null;
		try{
			resp.setContentType("text/html");
			PrintWriter writer = resp.getWriter();
//			ServletContext context = getServletContext();
//			URL filePath = context.getResource("res/Home.html");
			String line = "";
			String content = "";
			//D:/Academics/Upenn 2-1/IWS/Yaksh/search/resources/Home.html
			reader = new BufferedReader(new FileReader(new File("/opt/jetty/webapps/ROOT/res/Home.html")));
			while((line = reader.readLine()) != null){
				content += line;
			}
			writer.println(content);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		finally{
			if(reader != null){
				reader.close();
			}
		}
	}
}