package edu.upenn.cis455.search;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ImageServlet extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		OutputStream out = null;
		try{
			String fileRequested = req.getServletPath();
			if(fileRequested.endsWith(".png")){
				resp.setContentType("image/png");
			}
			else if(fileRequested.endsWith(".jpg") || fileRequested.endsWith(".jpeg")){
				resp.setContentType("image/jpeg");
			}
			else{
				resp.setContentType("image/jpeg");
			}
			
//			PrintWriter writer = resp.getWriter();
//			ServletContext context = getServletContext();
//			URL filePath = context.getResource(fileRequested);
			String path = "/opt/jetty/webapps/ROOT" + fileRequested;
			System.out.println(path);
			BufferedImage bi = ImageIO.read(new File(path));
			out = resp.getOutputStream();
			if(fileRequested.endsWith(".png")){
				ImageIO.write(bi, "png", out);
			}
			else if(fileRequested.endsWith(".jpg") || fileRequested.endsWith(".jpeg")){
				ImageIO.write(bi, "jpg", out);
			}
			else{
				ImageIO.write(bi, "jpg", out);
			}
		}
		catch(Exception e){
			System.err.println("Couldn't find the resource");
		}
		finally{
			if(out != null){
				out.close();
			}
		}
	}
}
