<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Product">
<head>
    <script src="styles/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="styles/bootstrap.js" type="text/javascript"></script>

    <link rel="stylesheet" href="styles/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="styles/home.css" type="text/css">

    <meta itemprop="name" content="Yaksh">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Yaksh</title>

</head>
<body class= "body">
    <div class="container">
	    <div class="row">
		    <div class="span6 offset2" id="searchdiv">
		        <h1><strong class="orange">Yaksh</strong></h1>
		        <form action="/search" method="GET" id="searchbox" class="well form-search">
  				    <input id="searchinput" type="text" name="search_query">
  				    <button type="submit" class="btn btn-warning"><i class="icon-search icon-white"></i> </button>
			    </form>
		    </div>
	    </div>

    <div class="navbar navbar-fixed-bottom">
        <div class="navbar-inner">
                <ul class="nav pull-left" data-no-collapse="true">
                    <li class="dropdown" data-no-collapse="true">
                        <a href="#" class="dropdown-toggle"data-toggle="dropdown">
                        About
                        <b class="caret"></b>
                        </a>
                        <div class="dropdown-menu infobox" data-no-collapse="true">
                         <p>A Search Engine built by Ashmeet Singh Rekhi, Nikhilesh Behra, Jatin Sharma & Swapnil Nankar.</p>
                        </div>
                    </li>
                    <li class="dropdown" data-no-collapse="true">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        Help
                        <b class="caret"></b>
                        </a>
                        <div class="dropdown-menu infobox" data-no-collapse="true">
                        <p>Help? We made this search engine in CIS555 under Professor Zack Ives, We would recommend taking this course for any help! :)</p>
                        </div>
                    </li>
                </ul>
            </div>
    </div>
</body>
</html>
